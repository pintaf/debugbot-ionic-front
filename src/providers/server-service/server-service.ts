import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';

import { Game } from '../../models/game';
import { Observer } from 'rxjs/Observer';

const MSG_TYPE_CLIENT_INFOS = 'client:infos';
const MSG_TYPE_CLIENT_NEW_GAME = 'client:new_game';
const MSG_TYPE_CLIENT_JOIN_GAME = 'client:join_game';
const MSG_TYPE_SERVER_GAMES = 'server:games';
const MSG_TYPE_SERVER_GAME_PPL_UPD= 'server:game:pplupd';
const MSG_TYPE_SERVER_GAME_CARDS= 'server:game:cards';
const MSG_TYPE_SERVER_GAME_PLAY= 'server:game:play';
const MSG_TYPE_SERVER_GAME_END= 'server:game:end';
const MSG_TYPE_SERVER_ERROR = 'server:error';
const MSG_TYPE_CLIENT_GAME_DECK_CHG = 'client:game:program';
const MSG_TYPE_CLIENT_GAME_DECK_RDY = 'client:game:compile';
const MSG_TYPE_CLIENT_GAME_END_TURN = 'client:game:stepover';

/*
  Generated class for the ServerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServerServiceProvider {

  public games: Game[] = [];

  constructor(private socket: Socket) {
    this.socket.on(MSG_TYPE_SERVER_GAMES, (games) => {
      console.log("games received from server: " + games);
      this.games = games;
    })

  }

  connect(username: string, avatarId: number): void{
    this.socket.connect();
    this.socket.emit(MSG_TYPE_CLIENT_INFOS, {name: username,avatarId: avatarId});
  }


}
