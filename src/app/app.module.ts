import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LobbyPage } from '../pages/lobby/lobby';
import { GameDetailsPage } from '../pages/game-details/game-details';
import { GameListPage } from '../pages/game-list/game-list';
import { NavProxyService } from '../providers/nav-proxy-service/nav-proxy-service';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { ServerServiceProvider } from '../providers/server-service/server-service';
const config: SocketIoConfig = { url: 'http://localhost:7777', options: {transports: ['websocket']} };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LobbyPage,
    GameListPage,
    GameDetailsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LobbyPage,
    GameListPage,
    GameDetailsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NavProxyService,
    ServerServiceProvider
  ]
})
export class AppModule {}
