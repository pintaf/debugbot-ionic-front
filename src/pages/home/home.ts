import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LobbyPage } from '../lobby/lobby';

import { ServerServiceProvider } from '../../providers/server-service/server-service'

import { Avatar, AVATARS } from '../../models/avatarList';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  avatars: Avatar[] = AVATARS;
  selectedAvatar: Avatar = null;
  username: string = null;

  constructor(public navCtrl: NavController, private toastCtrl: ToastController, private server: ServerServiceProvider) {

  }

  onAvatarSelect(avatar : Avatar) {
    this.selectedAvatar = avatar;
  }

  onEnterLobbyClicked() {
    if (this.selectedAvatar != null && this. username != null){
      this.server.connect(this.username, this.selectedAvatar.id);
      this.navCtrl.push(LobbyPage);
    } else {
      this.toastCtrl.create({
        message: 'Enter a username and select an avatar to proceed',
        duration: 3000,
        position: 'top'
      }).present();
    }
  }

}
