import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LobbyPage } from './lobby';
import { GameDetailsPage } from '../game-details/game-details';
import { GameListPage } from '../game-list/game-list';
import { NavProxyService } from '../../providers/nav-proxy-service/nav-proxy-service';

@NgModule({
  declarations: [
    LobbyPage,
    GameListPage,
    GameDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LobbyPage),
  ],entryComponents: [
    GameListPage,
    GameDetailsPage,
  ],
  providers: [
    NavProxyService
  ]
})
export class LobbyPageModule {}
