import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';

import {NavProxyService} from '../../providers/nav-proxy-service/nav-proxy-service';
import {GameDetailsPage} from '../game-details/game-details';
import {GameListPage} from '../game-list/game-list';

/**
 * Generated class for the LobbyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lobby',
  templateUrl: 'lobby.html',
})
export class LobbyPage {

  @ViewChild('detailNav') detailNav: Nav;
  @ViewChild('masterNav') masterNav: Nav;

  constructor(public navCtrl: NavController, public navParams: NavParams, private navProxy: NavProxyService) {
  }

  ionViewDidLoad() {
    // Add our nav controllers to
      // the nav proxy service...
      this.navProxy.masterNav = this.masterNav;
      this.navProxy.detailNav = this.detailNav;
      // set initial pages for
      // our nav controllers...
      this.masterNav.setRoot(GameListPage, 
        { detailNavCtrl: this.detailNav });
      this.detailNav.setRoot(GameDetailsPage);
  }

}
