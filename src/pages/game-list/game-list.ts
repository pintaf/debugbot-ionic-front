import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { GameDetailsPage } from '../game-details/game-details';
import { NavProxyService } from '../../providers/nav-proxy-service/nav-proxy-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { Game } from '../../models/game';

/**
 * Generated class for the GameListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-list',
  templateUrl: 'game-list.html',
})
export abstract class _MasterPage {

  private selectedGame: Game;

  constructor(public navCtrl: NavController, public navParams: NavParams, private navProxy: NavProxyService, public server: ServerServiceProvider) {

  }

  onGameSelect(game: Game) {
    this.selectedGame = game;
    this.navProxy.pushDetail(GameDetailsPage, game);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GameListPage');
  }



}

export class GameListPage extends _MasterPage {};