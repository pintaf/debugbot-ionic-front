import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Game } from '../../models/game';
import { ServerServiceProvider } from '../../providers/server-service/server-service';

/**
 * Generated class for the GameDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-details',
  templateUrl: 'game-details.html',
})
export abstract class _DetailPage {

  selectedGame: Game = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public server: ServerServiceProvider) {
    console.log("Game details Nav Params: ", navParams);
    this.selectedGame = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GameDetailsPage');
  }

}
export class GameDetailsPage extends _DetailPage {};
