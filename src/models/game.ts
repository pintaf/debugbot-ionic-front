import { Robot } from './robot';

export class Game {
  id: number;
  name: string;
  robots: Robot[];
  board;
  started: boolean;
  deck;
  currentTurn: number;
  types;
  maxPlayers: number;
}
