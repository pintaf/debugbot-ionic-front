export class Avatar {
  id: number;
  src: string;
  name: string;
}

export const AVATARS: Avatar[] = [
  { id: 1, src : "./assets/imgs/avatar_fluffy.png", name : "Fluf"},
  { id: 2, src : "./assets/imgs/avatar_lavander.png", name : "Lavy"},
  { id: 3, src : "./assets/imgs/avatar_silhouette.png", name : "Sil"},
  { id: 4, src : "./assets/imgs/avatar_skurts.png", name : "Skurt"},
]
