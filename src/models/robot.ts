export class Robot {
  id: number;
  name: string;
  color: string;
  fill: string;
  avatarId: number;
  position: { x: number, y: number };
  initialPosition = {};
  direction: number;
  health: number;
  cards: Array<any>;
  program: Array<any>;
  ready: boolean;
  types;
  animationEnded: boolean; 
  felt: boolean; 
  winner: boolean; 
}
